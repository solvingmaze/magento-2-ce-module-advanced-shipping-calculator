<?php

namespace Solvingmaze\AdvancedShippingCalculator\Plugin\Checkout\Block\Cart;

class LayoutProcessor
{
    public function __construct(\Magento\Framework\App\ProductMetadataInterface $productMetadata) {
        $this->productMetadata = $productMetadata;
    }

    public function getMagentoVersion() {
        return $this->productMetadata->getVersion();
    }

    /**
     * @param \Magento\Checkout\Block\Cart\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */

    public function afterProcess(
        \Magento\Checkout\Block\Cart\LayoutProcessor $subject,
        array $jsLayout
    ) {
        if (strpos($this->getMagentoVersion(), '2.0.') === false) {
            $jsLayout['components']['block-summary']['children']['block-shipping']['children']['address-fieldsets']['children']['city'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'options' => [],
                    'id' => 'city'
                ],
                'dataScope' => 'shippingAddress.city',
                'label' =>  __('City'),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => [],
                'sortOrder' => 200,
                'id' => 'city'
            ];
        }
        return $jsLayout;
    }
}

