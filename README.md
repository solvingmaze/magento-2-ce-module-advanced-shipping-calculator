# SolvingMaze Advanced Shipping Calculator Module for Magento 2 CE #

Visit [SolvingMaze Advanced Shipping Calculator](https://www.solvingmaze.com) for more info.

### Features ###

* Optimize UPS, FedEx, USPS and Spee-Dee shipping rates in Magento 2 CE
* Maximize package density and lower dimensional weight for shipping
* Lower packaging costs with free courier flat rate boxes


### Requirements ###

* Magento 2 Community Edition
* Composer


### Installation ###

1. Go to your Magento installation directory.

2. Install the module using composer:

    composer require solvingmaze/advanced-shipping-calculator-magento2-ce-module

3. Enable the module:

    bin/magento module:enable Solvingmaze_AdvancedShippingCalculator

4. Run the module installation script:

    bin/magento setup:upgrade


### Configuration ###

Configure the module by following the steps below.

#### A. Create Warehouse Profile ####

1. [Register for a free 30 day trial account](https://www.solvingmaze.com/account/register) at SolvingMaze.

2. Configure warehouse (e.g. add boxes, couriers, etc) by following [the short tutorial](http://www.solvingmaze.com/index/tutorial).

#### B. Configure Shipping Method ####

1. Log in to your Magento admin panel.

2. Go to Stores > Configuration > Sales > Shipping Method.

3. Enable SolvingMaze Advanced Shipping Calculator.

4. Enter Warehouse ID and API Key of your warehouse profile at SolvingMaze.

#### C. Configure Products ####

1. Log in to your Magento admin panel.

2. Go to Products > Catalog to edit products.

3. Enter product specification under Product Dimensions, Shippable Manufacturer Box, Void Space Dimensions, Prepacking, Stacking, and Packing Preferences.


### Questions ###

Contact us at support@solvingmaze.com