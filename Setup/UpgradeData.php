<?php
/**
 * Upgrade data model based on module version number and upgrade path.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Setup;
 
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Resource\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Script to upgrade data model.
 */
class UpgradeData extends AttributeCodes implements UpgradeDataInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Initialize script.
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory see Magento doc
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Update previously installed data model in database.
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup   See Magento doc
     * @param \Magento\Framework\Setup\ModuleContextInterface   $context See Magento doc
     *
     * @return void Nothing is returned
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
    }
}

