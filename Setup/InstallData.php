<?php
/**
 * Install data model to database.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Setup;
 
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Resource\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Script to install data model.
 */
class InstallData extends AttributeCodes implements InstallDataInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Initialize script.
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Install data model to database.
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup   See Magento doc
     * @param \Magento\Framework\Setup\ModuleContextInterface   $context See Magento doc
     *
     * @return void Nothing is returned
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        // validation rules at Magento/Ui/view/base/web/js/lib/validation/rules.js
        $dimClass = "validate-number validate-number-range number-range-0.001-9999999";
        $weightClass = $dimClass;
        $dimIncrClass = "validate-number validate-number-range number-range-0-9999999";
        $qtyClass = "validate-digits validate-digits-range digits-range-1-9999999";

        $this->addAttribute($eavSetup, self::ATTR_LENGTH, 10, self::DIMENSIONS, 'decimal', 'Length', 0.001, $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_WIDTH, 20, self::DIMENSIONS, 'decimal', 'Width', 0.001, $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_HEIGHT, 30, self::DIMENSIONS, 'decimal', 'Height', 0.001, $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_ALT_LENGTH, 40, self::DIMENSIONS, 'decimal', 'Alternative Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_ALT_WIDTH, 50, self::DIMENSIONS, 'decimal', 'Alternative Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_ALT_HEIGHT, 60, self::DIMENSIONS, 'decimal', 'Alternative Height', '', $dimClass);

        $this->addAttribute($eavSetup, self::ATTR_PACKAGING_LENGTH, 10, self::SHIPPABLE_BOX, 'decimal', 'Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PACKAGING_WIDTH, 20, self::SHIPPABLE_BOX, 'decimal', 'Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PACKAGING_HEIGHT, 30, self::SHIPPABLE_BOX, 'decimal', 'Height', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PACKAGING_WEIGHT, 40, self::SHIPPABLE_BOX, 'decimal', 'Gross Weight', '', $weightClass);
        $this->addAttribute($eavSetup, self::ATTR_PACKAGING_IRREGULAR, 50, self::SHIPPABLE_BOX, 'int', 'Not Regular Cardboard Box', false, '', 'boolean');

        $this->addAttribute($eavSetup, self::ATTR_VOID_FILLER, 10, self::VOID_SPACES, 'int', 'Pack into Void Space of Another Item', false, '', 'boolean');
        $this->addAttribute($eavSetup, self::ATTR_VOID_LENGTH1, 20, self::VOID_SPACES, 'decimal', 'Void Space 1 - Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_WIDTH1, 30, self::VOID_SPACES, 'decimal', 'Void Space 1 - Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_HEIGHT1, 40, self::VOID_SPACES, 'decimal', 'Void Space 1 - Height', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_LENGTH2, 50, self::VOID_SPACES, 'decimal', 'Void Space 2 - Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_WIDTH2, 60, self::VOID_SPACES, 'decimal', 'Void Space 2 - Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_HEIGHT2, 70, self::VOID_SPACES, 'decimal', 'Void Space 2 - Height', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_LENGTH3, 80, self::VOID_SPACES, 'decimal', 'Void Space 3 - Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_WIDTH3, 90, self::VOID_SPACES, 'decimal', 'Void Space 3 - Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_VOID_HEIGHT3, 100, self::VOID_SPACES, 'decimal', 'Void Space 3 - Height', '', $dimClass);

        $this->addAttribute($eavSetup, self::ATTR_PREPACK_MIN_QTY, 10, self::PREPACK, 'int', 'Minimum Trigger Quantity', '', $qtyClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_LENGTH1, 20, self::PREPACK, 'decimal', 'Prepackage 1 - Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_WIDTH1, 30, self::PREPACK, 'decimal', 'Prepackage 1 - Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_HEIGHT1, 40, self::PREPACK, 'decimal', 'Prepackage 1 - Height', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_WEIGHT1, 50, self::PREPACK, 'decimal', 'Prepackage 1 - Tare Weight', '', $weightClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_CAPACITY1, 60, self::PREPACK, 'int', 'Prepackage 1 - Maximum Capacity', '', $qtyClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_LENGTH2, 70, self::PREPACK, 'decimal', 'Prepackage 2 - Length', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_WIDTH2, 80, self::PREPACK, 'decimal', 'Prepackage 2 - Width', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_HEIGHT2, 90, self::PREPACK, 'decimal', 'Prepackage 2 - Height', '', $dimClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_WEIGHT2, 100, self::PREPACK, 'decimal', 'Prepackage 2 - Tare Weight', '', $weightClass);
        $this->addAttribute($eavSetup, self::ATTR_PREPACK_CAPACITY2, 110, self::PREPACK, 'int', 'Prepackage 2 - Maximum Capacity', '', $qtyClass);

        $this->addAttribute($eavSetup, self::ATTR_STACK_LENGTH_INC, 10, self::STACKING, 'decimal', 'Length Increment per Quantity', '', $dimIncrClass);
        $this->addAttribute($eavSetup, self::ATTR_STACK_WIDTH_INC, 20, self::STACKING, 'decimal', 'Width Increment per Quantity', '', $dimIncrClass);
        $this->addAttribute($eavSetup, self::ATTR_STACK_HEIGHT_INC, 30, self::STACKING, 'decimal', 'Height Increment per Quantity', '', $dimIncrClass);
        $this->addAttribute($eavSetup, self::ATTR_STACK_MAX_QTY, 40, self::STACKING, 'int', 'Maximum Stack Quantity', '', $qtyClass);

        $this->addAttribute($eavSetup, self::ATTR_UP, 10, self::PREFERENCES, 'int', 'This-Side-Up', false, '', 'boolean');
        $this->addAttribute($eavSetup, self::ATTR_SHIP_INDIVIDUALLY, 20, self::PREFERENCES, 'int', 'Pack Individually', false, '', 'boolean');
        $this->addAttribute($eavSetup, self::ATTR_GROUP, 30, self::PREFERENCES, 'int', 'Pack by Group', 0, '', 'select', 'Solvingmaze\AdvancedShippingCalculator\Model\Config\Source\GroupOptions');
        $this->addAttribute($eavSetup, self::ATTR_STRAPPABLE, 40, self::PREFERENCES, 'int', 'Strappable / Bundle', false, '', 'boolean');
        $this->addAttribute($eavSetup, self::ATTR_PREFER_CONTAINERS, 50, self::PREFERENCES, 'text', 'Prefer Containers', '', '');
        $this->addAttribute($eavSetup, self::ATTR_EXCLUDE_CONTAINERS, 60, self::PREFERENCES, 'text', 'Exclude Containers', '', '');
        $this->addAttribute($eavSetup, self::ATTR_PREFER_SERVICES, 70, self::PREFERENCES, 'text', 'Prefer Services', '', '');
    }

    /**
     * Add product attribute to database.
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetup      See Magento doc
     * @param string                             $id            Attribute code
     * @param int                                $sortOrder     Display sort order of attribute within group
     * @param string                             $group         Product attribute display group
     * @param string                             $type          The datababase column type
     * @param string                             $label         Input label for display
     * @param mixed                              $defaultVal    Default attribute value
     * @param string                             $validateClass Javascript validation rules
     * @param string                             $input         Input field type for display
     * @param mixed                              $source        Selection options
     *
     * @return void Nothing is returned.
     */
    protected function addAttribute($eavSetup, $id, $sortOrder, $group, $type, $label, $defaultVal, $validateClass, $input = 'text', $source = '') 
    {
        $eavSetup->removeAttribute(Product::ENTITY, $id);
        $eavSetup->addAttribute(
            Product::ENTITY,
            $id,
            [
                'group'                   => $group,
                'type'                    => $type,
                'label'                   => $label,
                'default'                 => $defaultVal,
                'required'                => false,
                'frontend_class'          => $validateClass,
                'input'                   => $input,
                'source'                  => $source,
                'sort_order'              => $sortOrder,
                'global'                  => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'user_defined'            => true,
                'used_in_product_listing' => false,
                'unique'                  => false,
                'visible_on_front'        => false,
                'apply_to'                => 'simple,configurable',
            ]
        );
    }
}

