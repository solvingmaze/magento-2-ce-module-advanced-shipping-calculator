<?php
/**
 * Delete installed data model from database.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Script to delete data model.
 */
class Uninstall extends AttributeCodes implements UninstallInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Initialize script.
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory See Magento doc
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Uninstall data model from database.
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup   See Magento doc
     * @param \Magento\Framework\Setup\ModuleContextInterface $context See Magento doc
     *
     * @return void Nothing is returned
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create();

        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_LENGTH);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_WIDTH);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_HEIGHT);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_ALT_LENGTH);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_ALT_WIDTH);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_ALT_HEIGHT);

        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PACKAGING_LENGTH);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PACKAGING_WIDTH);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PACKAGING_HEIGHT);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PACKAGING_WEIGHT);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PACKAGING_IRREGULAR);

        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_FILLER);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_LENGTH1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_WIDTH1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_HEIGHT1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_LENGTH2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_WIDTH2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_HEIGHT2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_LENGTH3);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_WIDTH3);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_VOID_HEIGHT3);

        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_MIN_QTY);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_LENGTH1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_WIDTH1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_HEIGHT1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_WEIGHT1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_CAPACITY1);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_LENGTH2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_WIDTH2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_HEIGHT2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_WEIGHT2);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREPACK_CAPACITY2);

        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_STACK_LENGTH_INC);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_STACK_WIDTH_INC);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_STACK_HEIGHT_INC);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_STACK_MAX_QTY);

        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_UP);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_SHIP_INDIVIDUALLY);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_GROUP);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_STRAPPABLE);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREFER_CONTAINERS);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_EXCLUDE_CONTAINERS);
        $eavSetup->removeAttribute(Product::ENTITY, self::ATTR_PREFER_SERVICES);
    }
}



