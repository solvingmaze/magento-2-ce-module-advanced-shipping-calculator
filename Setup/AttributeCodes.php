<?php
/**
 * Product attribute group names and attribute names.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Setup;
 
/**
 * Define constants for product attribute group names and attribute names.
 */
class AttributeCodes
{
    const DIMENSIONS = 'Product Dimensions';
    const SHIPPABLE_BOX = 'Shippable Manufacturer Box';
    const VOID_SPACES = 'Void Space Dimensions';
    const PREPACK = 'Prepacking';
    const STACKING = 'Stacking';
    const PREFERENCES = 'Packing Preferences';

    const ATTR_LENGTH = 'sm_length';
    const ATTR_WIDTH = 'sm_width';
    const ATTR_HEIGHT = 'sm_height';
    const ATTR_ALT_LENGTH = 'sm_alt_length';
    const ATTR_ALT_WIDTH = 'sm_alt_width';
    const ATTR_ALT_HEIGHT = 'sm_alt_height';

    const ATTR_PACKAGING_LENGTH = 'sm_packaging_length';
    const ATTR_PACKAGING_WIDTH = 'sm_packaging_width';
    const ATTR_PACKAGING_HEIGHT = 'sm_packaging_height';
    const ATTR_PACKAGING_WEIGHT = 'sm_packaging_weight';
    const ATTR_PACKAGING_IRREGULAR = 'sm_packaging_irregular';

    const ATTR_VOID_FILLER = 'sm_void_filler';
    const ATTR_VOID_LENGTH1 = 'sm_void_length1';
    const ATTR_VOID_WIDTH1 = 'sm_void_width1';
    const ATTR_VOID_HEIGHT1 = 'sm_void_height1';
    const ATTR_VOID_LENGTH2 = 'sm_void_length2';
    const ATTR_VOID_WIDTH2 = 'sm_void_width2';
    const ATTR_VOID_HEIGHT2 = 'sm_void_height2';
    const ATTR_VOID_LENGTH3 = 'sm_void_length3';
    const ATTR_VOID_WIDTH3 = 'sm_void_width3';
    const ATTR_VOID_HEIGHT3 = 'sm_void_height3';

    const ATTR_PREPACK_MIN_QTY = 'sm_prepack_min_qty';
    const ATTR_PREPACK_LENGTH1 = 'sm_prepack_length1';
    const ATTR_PREPACK_WIDTH1 = 'sm_prepack_width1';
    const ATTR_PREPACK_HEIGHT1 = 'sm_prepack_height1';
    const ATTR_PREPACK_WEIGHT1 = 'sm_prepack_tare_weight1';
    const ATTR_PREPACK_CAPACITY1 = 'sm_prepack_capacity1';
    const ATTR_PREPACK_LENGTH2 = 'sm_prepack_length2';
    const ATTR_PREPACK_WIDTH2 = 'sm_prepack_width2';
    const ATTR_PREPACK_HEIGHT2 = 'sm_prepack_height2';
    const ATTR_PREPACK_WEIGHT2 = 'sm_prepack_tare_weight2';
    const ATTR_PREPACK_CAPACITY2 = 'sm_prepack_capacity2';

    const ATTR_STACK_LENGTH_INC = 'sm_prepack_stack_length_inc';
    const ATTR_STACK_WIDTH_INC = 'sm_prepack_stack_width_inc';
    const ATTR_STACK_HEIGHT_INC = 'sm_prepack_stack_height_inc';
    const ATTR_STACK_MAX_QTY = 'sm_prepack_stack_max_qty';

    const ATTR_UP = 'sm_up';
    const ATTR_SHIP_INDIVIDUALLY = 'sm_ship_individually';
    const ATTR_GROUP = 'sm_group';
    const ATTR_STRAPPABLE = 'sm_strappable';
    const ATTR_PREFER_CONTAINERS = 'sm_prefer_containers';
    const ATTR_EXCLUDE_CONTAINERS = 'sm_exclude_containers';
    const ATTR_PREFER_SERVICES = 'sm_prefer_services';
}

