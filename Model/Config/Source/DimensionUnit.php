<?php
/**
 * Dimension units.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Model\Config\Source;

/**
 * Define dimension units for shipping method configuration.
 */
class DimensionUnit
{
    /**
     * Return dimension units.
     *
     * @return array Associated array of dimension unit type and label
     */
    public function toOptionArray()
    {
        $arr = array();
        $arr['in'] = 'Inches';
        $arr['cm'] = 'Centimeters';
        return $arr;
    }
}

