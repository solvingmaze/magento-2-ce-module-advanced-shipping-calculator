<?php
/**
 * Shipping destination address types.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Model\Config\Source;

/**
 * Define destination address types for shipping method configuration.
 */
class AddressType
{
    /**
     * Return shipping address types.
     *
     * @return array Associated array of address type and label
     */
    public function toOptionArray()
    {
        $arr = array();
        $arr['residential'] = 'Residential';
        $arr['commercial'] = 'Commercial';
        return $arr;
    }
}

