<?php
/**
 * Packing group options for products.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

/**
 * Define packing group options for product group custom attribute.
 */
class GroupOptions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Return product group options.
     *
     * @return array Array of group label and value
     */
    public function getAllOptions()
    {
        $this->_options = array(['label'=>'0', 'value'=>0],
                          ['label'=>'1', 'value'=>1],
                          ['label'=>'2', 'value'=>2]
                      );
        return $this->_options;
    }
 
    /**
     * Get text for given option value.
     *
     * @param string|integer $value Group option value
     *
     * @return string|bool Group option label
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] === $value) {
                return $option['label'];
            }
        }
        return false;
    }
 
    /**
     * Retrieve flat column definition.
     *
     * @return array Attribute properties
     */
    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        return [
            $attributeCode => [
                'unsigned' => true,
                'default' => 0,
                'extra' => null,
                'type' => Table::TYPE_INTEGER,
                'nullable' => false,
                'comment' => "Custom Attribute Group $attributeCode column",
            ],
        ];
    }
}


