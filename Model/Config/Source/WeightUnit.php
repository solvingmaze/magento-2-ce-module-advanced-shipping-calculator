<?php
/**
 * Weight units.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */

namespace Solvingmaze\AdvancedShippingCalculator\Model\Config\Source;

/**
 * Define weight units for shipping method configuration.
 */
class WeightUnit
{
    /**
     * Return dimension units.
     *
     * @return array Associated array of weight unit type and label
     */
    public function toOptionArray()
    {
        $arr = array();
        $arr['lb'] = 'Pounds';
        $arr['oz'] = 'Ounces';
        $arr['kg'] = 'Kilograms';
        $arr['g'] = 'Grams';
        return $arr;
    }
}
