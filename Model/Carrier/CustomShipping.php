<?php

/**
 * Calculate shipping rates via SolvingMaze Advanced Shipping Calculator API.
 *
 * @category SolvingMaze
 * @package  Solvingmaze_AdvancedShippingCalculator
 * @author   SolvingMaze <support@solvingmaze.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link     http://www.solvingmaze.com
 */
namespace Solvingmaze\AdvancedShippingCalculator\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Shipping method model.
 */
class CustomShipping extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @var Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Initialize custom shipping.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface          $scopeConfig       See Magento doc
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory  $rateErrorFactory  See Magento doc
     * @param \Psr\Log\LoggerInterface                                    $logger            See Magento doc
     * @param \Magento\Shipping\Model\Rate\ResultFactory                  $rateResultFactory See Magento doc
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory See Magento doc
     * @param \Magento\Catalog\Api\ProductRepositoryInterface             $productRepository See Magento doc
     * @param array                                                       $data              See Magento doc
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->productRepository = $productRepository;
        $this->_code = 'solvingmaze_advancedshippingcalculator';
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Collect shipping rates.
     *
     * @param RateRequest $request Request object
     *
     * @return                                      \Magento\Shipping\Model\Rate\Result
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        // get destination
        $dest = array();
        $dest['country'] = $request->getDestCountryId();
        $dest['region'] = $request->getDestRegionCode();
        $dest['post'] = $request->getDestPostcode();
        $dest['city'] = $request->getDestCity();
        $dest['residential'] = strtolower($this->getConfigData('dest_type')) === 'residential';
        if (!($dest['country'] && $dest['post'])) {
            return false;
        }

        // get API key and warehouse ID from shipping method configuration
        $apiKey = $this->getConfigData('api_key');
        $warehouseId = $this->getConfigData('warehouse_id');
        if (!($apiKey && $warehouseId)) {
            return false;
        }

        // get weight and dimension unit from shipping method configuration
        $weightUnit = $this->getConfigData('weight_unit');
        $dimensionUnit = $this->getConfigData('dimension_unit');

        // get cart items
        $itemsArr = array();
        $items = $request->getAllItems();
        foreach ($items as $item) {
            $product = $this->productRepository->getById($item->getProductId());
            if ($product->getTypeId() === 'simple' && !$item->getFreeShipping() && !$item->getIsQtyDecimal()) {
                // init item array for API call
                $itemArr = array();
                $itemArr['sku'] = $item->getSku();
                $itemArr['name'] = $item->getName();
                $itemArr['weightUnit'] = $weightUnit;
                $itemArr['dimensionUnit'] = $dimensionUnit;
                $itemArr['qty'] = $item->getQty();
                // set default price if not defined
                $unitPrice = $item->getPrice();
                $itemArr['price'] = $unitPrice ? $unitPrice : 0;
                // set default weight if not defined
                $weight = $item->getWeight();
                $itemArr['weight'] = $weight ? $weight : 0.001;
                // product dimensions
                $dims = array();
                $dim = array();
                $dim['length'] = $this->getAttributeValue($product->getData('sm_length'), 0.001);
                $dim['width'] = $this->getAttributeValue($product->getData('sm_width'), 0.001);
                $dim['height'] = $this->getAttributeValue($product->getData('sm_height'), 0.001);
                $dims[] = $dim;
                $altLength = $this->getAttributeValue($product->getData('sm_alt_length'), null);
                $altWidth = $this->getAttributeValue($product->getData('sm_alt_width'), null);
                $altHeight = $this->getAttributeValue($product->getData('sm_alt_height'), null);
                if ($altLength && $altWidth && $altHeight) {
                    $dim = array();
                    $dim['length'] = $altLength;
                    $dim['width'] = $altWidth;
                    $dim['height'] = $altHeight;
                    $dims[] = $dim;
                }
                $itemArr['dimensions'] = $dims;
                // shippable manufacturer box
                $boxLength = $this->getAttributeValue($product->getData('sm_packaging_length'), null);
                $boxWidth = $this->getAttributeValue($product->getData('sm_packaging_width'), null);
                $boxHeight = $this->getAttributeValue($product->getData('sm_packaging_height'), null);
                $boxWeight = $this->getAttributeValue($product->getData('sm_packaging_weight'), null);
                $boxIrregular = $this->getAttributeValue($product->getData('sm_packaging_irregular'), false);
                if ($boxLength && $boxWidth && $boxHeight && $boxWeight) {
                    $packaging = array();
                    $packaging['length'] = $boxLength;
                    $packaging['width'] = $boxWidth;
                    $packaging['height'] = $boxHeight;
                    $packaging['weight'] = $boxWeight;
                    $packaging['irregular'] = $boxIrregular;
                    $itemArr['packaging'] = $packaging;
                }
                // void spaces
                $itemArr['voidFiller'] = $this->getAttributeValue($product->getData('sm_void_filler'), false);
                $voidDimensions = array();
                $voidLength1 = $this->getAttributeValue($product->getData('sm_void_length1'), null);
                $voidWidth1 = $this->getAttributeValue($product->getData('sm_void_width1'), null);
                $voidHeight1 = $this->getAttributeValue($product->getData('sm_void_height1'), null);
                if ($voidLength1 && $voidWidth1 && $voidHeight1) {
                    $voidDim = array();
                    $voidDim['length'] = $voidLength1;
                    $voidDim['width'] = $voidWidth1;
                    $voidDim['height'] = $voidHeight1;
                    $voidDimensions[] = $voidDim;
                }
                $voidLength2 = $this->getAttributeValue($product->getData('sm_void_length2'), null);
                $voidWidth2 = $this->getAttributeValue($product->getData('sm_void_width2'), null);
                $voidHeight2 = $this->getAttributeValue($product->getData('sm_void_height2'), null);
                if ($voidLength2 && $voidWidth2 && $voidHeight2) {
                    $voidDim = array();
                    $voidDim['length'] = $voidLength2;
                    $voidDim['width'] = $voidWidth2;
                    $voidDim['height'] = $voidHeight2;
                    $voidDimensions[] = $voidDim;
                }
                $voidLength3 = $this->getAttributeValue($product->getData('sm_void_length3'), null);
                $voidWidth3 = $this->getAttributeValue($product->getData('sm_void_width3'), null);
                $voidHeight3 = $this->getAttributeValue($product->getData('sm_void_height3'), null);
                if ($voidLength3 && $voidWidth3 && $voidHeight3) {
                    $voidDim = array();
                    $voidDim['length'] = $voidLength3;
                    $voidDim['width'] = $voidWidth3;
                    $voidDim['height'] = $voidHeight3;
                    $voidDimensions[] = $voidDim;
                }
                if (count($voidDimensions)) {
                    $itemArr['voidDimensions'] = $voidDimensions;
                }
                // prepacking
                $prepackages = array();
                $prepackLength1 = $this->getAttributeValue($product->getData('sm_prepack_length1'), null);
                $prepackWidth1 = $this->getAttributeValue($product->getData('sm_prepack_width1'), null);
                $prepackHeight1 = $this->getAttributeValue($product->getData('sm_prepack_height1'), null);
                $prepackWeight1 = $this->getAttributeValue($product->getData('sm_prepack_tare_weight1'), null);
                $prepackCapacity1 = $this->getAttributeValue($product->getData('sm_prepack_capacity1'), null);
                if ($prepackLength1 && $prepackWidth1 && $prepackHeight1 && $prepackCapacity1) {
                    $prepackage = array();
                    $prepackage['length'] = $prepackLength1;
                    $prepackage['width'] = $prepackWidth1;
                    $prepackage['height'] = $prepackHeight1;
                    $prepackage['weight'] = $prepackWeight1;
                    $prepackage['capacity'] = $prepackCapacity1;
                    $prepackages[] = $prepackage;
                }
                $prepackLength2 = $this->getAttributeValue($product->getData('sm_prepack_length2'), null);
                $prepackWidth2 = $this->getAttributeValue($product->getData('sm_prepack_width2'), null);
                $prepackHeight2 = $this->getAttributeValue($product->getData('sm_prepack_height2'), null);
                $prepackWeight2 = $this->getAttributeValue($product->getData('sm_prepack_tare_weight2'), null);
                $prepackCapacity2 = $this->getAttributeValue($product->getData('sm_prepack_capacity2'), null);
                if ($prepackLength2 && $prepackWidth2 && $prepackHeight2 && $prepackCapacity2) {
                    $prepackage = array();
                    $prepackage['length'] = $prepackLength2;
                    $prepackage['width'] = $prepackWidth2;
                    $prepackage['height'] = $prepackHeight2;
                    $prepackage['weight'] = $prepackWeight2;
                    $prepackage['capacity'] = $prepackCapacity2;
                    $prepackages[] = $prepackage;
                }
                if (count($prepackages)) {
                    $itemArr['prepackageMinQty'] = $this->getAttributeValue($product->getData('sm_prepack_min_qty'), 1);
                    $itemArr['prepackages'] = $prepackages;
                }
                // stacking
                $stackLengthInc = $this->getAttributeValue($product->getData('sm_prepack_stack_length_inc'), null);
                $stackWidthInc = $this->getAttributeValue($product->getData('sm_prepack_stack_width_inc'), null);
                $stackHeightInc = $this->getAttributeValue($product->getData('sm_prepack_stack_height_inc'), null);
                $stackMaxQty = $this->getAttributeValue($product->getData('sm_prepack_stack_max_qty'), null);
                if ($stackLengthInc && $stackWidthInc && $stackHeightInc && $stackMaxQty) {
                    $stack = array();
                    $stack['lengthIncrement'] = $stackLengthInc;
                    $stack['widthIncrement'] = $stackWidthInc;
                    $stack['heightIncrement'] = $stackHeightInc;
                    $stack['maxQty'] = $stackMaxQty;
                    $itemArr['stack'] = $stack;
                }
                // packing preferences
                $itemArr['rotatable'] = !$this->getAttributeValue($product->getData('sm_up'), false);
                $itemArr['packable'] = !$this->getAttributeValue($product->getData('sm_ship_individually'), false);
                $itemArr['group'] = $this->getAttributeValue($product->getData('sm_group'), 0);
                $itemArr['strappable'] = $this->getAttributeValue($product->getData('sm_strappable'), false);
                // container preferences
                $preferContainers = $this->getAttributeValue($product->getData('sm_prefer_containers'), null);
                if ($preferContainers) {
                    $itemArr['preferContainers'] = $preferContainers;
                }
                $excludeContainers = $this->getAttributeValue($product->getData('sm_exclude_containers'), null);
                if ($excludeContainers) {
                    $itemArr['excludeContainers'] = $excludeContainers;
                }
                // service preferences
                $preferServices = $this->getAttributeValue($product->getData('sm_prefer_services'), null);
                if ($preferServices) {
                    $itemArr['preferServices'] = $preferServices;
                }

                $itemsArr[] = $itemArr;
            }
        }

        // init API input parameters
        $data = array(
                'items' => $itemsArr,
                'destination' => $dest,
                'showRates' => true,
                'magento' => '2.1'
                );

        // submit API call
        $url = "http://api.solvingmaze.com/calculate/key/$apiKey/warehouse/".rawurlencode($warehouseId);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60 * 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60 * 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $retVal = curl_exec($ch);
        curl_close($ch);
        if ($retVal) {
            $jsonVal = json_decode($retVal, true);
            $status = $jsonVal['status'];
            if ($status['success']) {
                // result is an object of Magento\Shipping\Model\Rate\Result
                $result = $this->rateResultFactory->create();
                foreach ($jsonVal['services'] as $service) {
                    if ($service['status']['success']) {
                        $carrierName = $service['carrier'];
                        $serviceName = $service['service'];
                        $rate = $service['rate'];
                        $method = $this->rateMethodFactory->create();
                        if (in_array(strtolower($carrierName), array('ups', 'fedex', 'usps'))) {
                            $method->setCarrier(strtolower($carrierName));
                        }
                        else {
                            $method->setCarrier('ups');
                        }
                        $method->setCarrierTitle($carrierName);
                        $method->setMethod(strtolower($serviceName));
                        $method->setMethodTitle($serviceName);
                        $method->setPrice($rate);
                        $method->setCost($rate);
                        $result->append($method);
                    }
                }
                return $result;
            } else {
                $this->logMe(implode(" ", $status['messages']));

                return false;
            }
        } else {
            $this->logMe("pack and quote api returned null");

            return false;
        }
    }

    /**
     * Return given value if not null, otherwise, return default value.
     *
     * @param string $val        Non-null value to return
     * @param mixed  $defaultVal Default value to return if givne value is null
     *
     * @return mixed
     */
    protected function getAttributeValue($val, $defaultVal)
    {
        return $val? $val: $defaultVal;
    }

    /**
     * Log variable content to debug log.
     *
     * @param mixed $msg Variable to be dumped
     *
     * @return void Nothing is returned
     */
    protected function logMe($msg)
    {
        $this->_logger->addDebug(var_export($msg, true));
    }

    /**
     * Get allowed shipping methods.
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }
}

